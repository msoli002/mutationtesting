package edu.odu.cs.cs350.yellow1.jar;

public enum LogCreationState {
	/**
	 * The log directory is to be created
	 */
	createLogDir,
	/**
	 * The lof directory exists already
	 */
	logDirExists
}
